# Fresh Install

When setting up a fresh installation of a Debian/Ubuntu based Distro use the
`post_install` script found under the dotfiles folder after copying the folder
into your /home/USERNAME directory. So the symlinking will work as intended.

